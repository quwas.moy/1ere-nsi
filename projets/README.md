## Projets 

| Groupes                  | Projets                |
| ------------------------ | ---------------------- |
| Kaiss - Pierre - Malcolm | Bingo                  |
| Hamid - Lucas            | Roulette Russe         |
| Nicolas - Nassim         | Plateforme Web / Quizz |
| Bilal - Samir            | Morpion                |
| Lilya - Adam             | 4 images - 1 mot       |
| Samy - Guillaume         | Bataille Navale        |

Projets vus en classe:

- [Mastermind](mastermind.py)

- [Pendu](pendu.py)

  